"""
Author: Kwanele Ndhlovu
Name: utils.py
Description: Utilities file which contains all the code required by the project to communicate with the ml model
"""
from pathlib import Path
import pickle
import os
import xgboost as xgb
from sklearn.preprocessing import LabelEncoder
from joblib import dump, load
import pandas as pd

path = Path(__file__).parent


class MainProcessor:
    """
    Main Processor of Fraudzilla
    """

    def __init__(self, data):
        self.raw_user_params = data
        self.data = data['data']
        self.flag = data['flag']
        self.model_path = path/'fraudmodel/model.pkl'
        self.fraud_model = None
        self.raw_user_input = None
        self.user_df = None
        self.allowed_extentions = ['.csv']
        self.caught_error = {}
        self.allowed_chars = 'AaBbCcDdEeFfGgHhJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz'
        self.allowed_nums = '123456789'
        self.white_list_chars = [ord(i) for i in list(self.allowed_chars)]
        self.white_list_nums = [ord(i) for i in list(self.allowed_nums)]
        self.education_keys = {
            'high_school': 0,
            'college': 1,
            'masters': 2,
            'phd': 3
        }
        self.int_values = ['age', 'property_claim', 'witnesses',
                           'capital_gain', 'capital_loss', 'total_claim_amount', 'policy_annual_premium', 'months_as_customer', 'injury_claim']
        self.cat_values = ['police_report']
        self.full_columns = self.int_values + self.cat_values
        self.bad_chars = []
        self.trans_df_list = []
        self.error = False
        self.raw_results = self.process_single_transcation(
        ) if data['flag'] == "single" else self.process_data_file()

    def to_save(self):
        pass

    def process_data_file(self):
        try:
            data_file = self.raw_user_params['dataFileUrl']
            if data_file.name.endswith('.csv'):
                raw_user_df = pd.read_csv(data_file)
                dataset_cols = [i for i in raw_user_df.columns]
                if self.full_colums == dataset_cols:
                    for row in range(len(raw_user_df.index)):
                        self.process_single_transcation(
                            data=raw_user_df, rownum=row, fromFile=True)
                else:
                    pass
        except Exception as e:
            print(e)

    def sanitize_int_values(self, value):
        try:
            self.bad_chars = []
            error_state = False
            raw_input = [ord(i) for i in list(value)]
            if len(raw_input) == 1 or len(raw_input) == 2:
                for i in raw_input:
                    if i not in self.white_list_nums:
                        self.bad_chars.append(chr(i))
                        error_state = True
            else:
                error_state = True
            return error_state
        except Exception as e:
            print(e)
            return error_state

    def sanitize_str_values(self, value):
        try:
            self.bad_chars = []
            raw_input = [ord(i) for i in list(value)]
            for i in raw_input:
                if i not in self.white_list_chars:
                    self.bad_chars.append(chr(i))
            return False if len(self.bad_chars) == 0 else True
        except Exception as e:
            return True
            print(e)

    def apply_to_transforms(self, df):
        for i in self.cat_values:
            df[i] = df[i].astype('category')
        return df

    def process_single_transcation(self, data=None, rownum=0, fromFile=False):
        try:
            if not data:
                data = self.data
            self.raw_user_input = {}
            for col in self.full_columns:
                self.raw_user_input[col] = [data[col] if self.flag ==
                                            "single" else data.at[rownum, col]]
            # for i in self.raw_user_input.keys():
            #     if i in self.int_values:
            #         if not self.sanitize_int_values(self.raw_user_input[i][0]):
            #             self.caught_error[i] = self.bad_chars
            #     if i in self.str_values:
            #         if not self.sanitize_str_values(self.raw_user_input[i][0]):
            #             self.caught_error[i] = self.bad_chars
            if len(self.caught_error) == 0:
                single_trans_df = self.apply_to_transforms(
                    pd.DataFrame.from_dict(self.raw_user_input))
                self.trans_df_list.append(single_trans_df)
            # self.error = True
        except Exception as e:
            self.error = True
            print(e)

    def load_model(self):
        try:
            print(str(self.model_path))
            if os.path.isfile(str(self.model_path)):
                with open(str(self.model_path), 'rb') as model_file:
                    self.fraud_model = pickle.load(model_file)
                model_file.close()
                # self.fraud_model = load(str(self.model_path))
            else:
                self.error = True
        except Exception as e:
            print(e)
            self.error = True

    def execute_model(self):
        try:
            self.load_model()
            df = self.trans_df_list[0]
            pred_cols = list(self.trans_df_list[0].columns.values)[:-1]
            pred = pd.Series(self.fraud_model.predict(df[pred_cols]))
            print(pred)
            # print(pred_cols)
            # results = self.fraud_model.predict(d_test)
            # run the prediction
            # self.raw_results = self.fraud_model.predict(self.user_df)
        except Exception as e:
            self.error = True
            print(e)
